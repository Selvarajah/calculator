/* Author: Jekathmenan Selvarajah
 * Project: Programmierwochen_Casino To change this license header, choose License Headers in Project Properties.
 * Project: Programmierwochen_Casino To change this template file, choose Tools | Templates
 * Project: Programmierwochen_Casino and open the template in the editor.
 */
package calculator.Model;

import calculator.Calculator;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.math.BigDecimal;
import calculator.Model.Operand;

/**
 *
 * @author Jekathmenan
 */
public class CalculatorModel
{
    private final PropertyChangeSupport changes = new PropertyChangeSupport(this);
    private Calculator mainApp;
    
    private Operand state = null;
    
    private String number1 = "";
    private String number2 = "";
    
    private BigDecimal nr1 = null;
    private BigDecimal nr2 = null;
    private boolean choosenOne = true;

    /**
     * 
     * A constructor to initialize mainApp
     * 
     * @param mainApp 
     */
    public CalculatorModel(Calculator mainApp)
    {
        this.mainApp = mainApp;
    }

    /**
     * 
     * A method to exit the view --> Calls mainApps exit()
     * 
     */
    public void exitAction()
    {
        mainApp.exit();
    }

    /**
     * 
     * A method to minimize Window
     * 
     */
    public void minimizeAction()
    {
        try
        {
            mainApp.minimizeView();
        } catch (IOException ex)
        {

        }
    }
    
    /**
     * 
     * A method to change state+
     * 
     * @param operand 
     */
    public void operationAction(Operand operand)
    {
        if(!choosenOne)
        {
            
            this.state = operand;
            nr2 = new BigDecimal(number2);
            choosenOne = true;
            
            //calculating the result
            nr1 = calculate();
            
            output(1);
        }
        else
        {
            this.state = operand;
            nr1 = new BigDecimal(number1);
            choosenOne = false;
        }
    }
    
    public void numberAction(int number)
    {
        if(choosenOne)
        {
            number1 += number;
        }
        else
        {
            number2 += number;
        }
    }
    
    public void equalsAction()
    {
        
        // setting the value
        BigDecimal result = calculate();
    }
    
    public BigDecimal calculate()
    {
        BigDecimal result;
        switch(state)
        {
            case ADDITION:
                result = nr1.add(nr2);
            case SUBTRACTION:
                result = nr1.subtract(nr2);
            case MULTIPLICATION:
                result = nr1.multiply(nr2);
            case DIVISION:
                result = nr1.divide(nr2);
        }
        
        return null;
    }
    
    public void output(int value)
    {
        
    }

    public void addPropertyChangeListener(PropertyChangeListener listener)
    {
        changes.addPropertyChangeListener(listener);
    }
}

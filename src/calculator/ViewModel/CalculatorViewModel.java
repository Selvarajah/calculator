/* Author: Jekathmenan Selvarajah
 * Project: Programmierwochen_Casino To change this license header, choose License Headers in Project Properties.
 * Project: Programmierwochen_Casino To change this template file, choose Tools | Templates
 * Project: Programmierwochen_Casino and open the template in the editor.
 */

package calculator.ViewModel;

import calculator.Model.CalculatorModel;
import calculator.Model.Operand;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Jekathmenan
 */
public class CalculatorViewModel implements PropertyChangeListener
{
    private final CalculatorModel model;
    private StringProperty output = new SimpleStringProperty();

    public CalculatorViewModel(CalculatorModel model)
    {
        this.model = model;
    }
    
    public void exitAction()
    {
        System.out.println("exitaction works");
        model.exitAction();
        
    }
    
    public void minimizeAction()
    {
        System.out.println("minimizeAction");
        model.minimizeAction();
    }
    
    public void operandAction(Operand operand)
    {
        System.out.println("operandAction");
        model.operationAction(operand);
    }
    
    public void numberAction(int number)
    {
        model.numberAction(number);
    }
    
    public void equalsAction()
    {
        model.equalsAction();
    }
    
    @Override
    public void propertyChange(PropertyChangeEvent evt)
    {
        switch(evt.getPropertyName())
        {
            case "output":
                output.set(evt.getNewValue().toString());
        }
    }

    public StringProperty getOutput()
    {
        return output;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import calculator.Model.CalculatorModel;
import calculator.View.CalculatorView;
import calculator.ViewModel.CalculatorViewModel;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.swing.text.View;

/**
 *
 * @author Jekathmenan
 */
public class Calculator extends Application
{
    Stage stage;
    @Override
    public void start(Stage primaryStage) throws IOException
    {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("View/CalculatorView.fxml"));
        Parent root = loader.load();
        
        CalculatorView view = loader.getController();
        CalculatorModel model = new CalculatorModel(this);
        final CalculatorViewModel viewModel = new CalculatorViewModel(model);
        view.setViewModel(viewModel);
        model.addPropertyChangeListener(viewModel);
        view.bind();
        
        //
        stage = primaryStage;
        
        // All the binding need to be done here
        
        Scene scene = new Scene(root);
        
        // setting scene to not resizable and undecorated
        primaryStage.setResizable(false);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    public void minimizeView() throws IOException
    {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("View/CalculatorView.fxml"));
        Parent root = loader.load();
        
        CalculatorView view = loader.getController();
        CalculatorModel model = new CalculatorModel(this);
        final CalculatorViewModel viewModel = new CalculatorViewModel(model);
        view.setViewModel(viewModel);
        model.addPropertyChangeListener(viewModel);
        view.bind();
        
        
        // All the binding need to be done here
        
        Scene scene = new Scene(root);
        
        // setting scene to not resizable and undecorated
        stage.setResizable(false);
        //stage.initStyle(StageStyle.UNDECORATED);
        
        // minimize window
        stage.setIconified(true);
        
        stage.setScene(scene);
        stage.show();
    }
    
    public void exit()
    {
        System.exit(0);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }
    
}

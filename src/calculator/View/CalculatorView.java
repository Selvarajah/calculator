/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator.View;

import calculator.ViewModel.CalculatorViewModel;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Jekathmenan
 */
public class CalculatorView implements Initializable
{

    
    @FXML
    private Label lblOutput;
    
    //
    private CalculatorViewModel viewModel;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        // TODO
    }
    
    public void bind()
    {
        lblOutput.textProperty().bind(viewModel.getOutput());
        
    }

    @FXML
    private void exitAction(MouseEvent event)
    {
        viewModel.exitAction();
    }

    @FXML
    private void minimizeAction(MouseEvent event)
    {
        viewModel.minimizeAction();
    }

    @FXML
    private void zeroAction(ActionEvent event)
    {
        viewModel.numberAction(0);
    }

    @FXML
    private void oneAction(ActionEvent event)
    {
        viewModel.numberAction(1);
    }

    @FXML
    private void fourAction(ActionEvent event)
    {
    }

    @FXML
    private void twoAction(ActionEvent event)
    {
    }

    @FXML
    private void sixAction(ActionEvent event)
    {
    }

    @FXML
    private void minusAction(ActionEvent event)
    {
    }

    @FXML
    private void plusAction(ActionEvent event)
    {
    }

    @FXML
    private void equalsAction(ActionEvent event)
    {
    }

    @FXML
    private void fiveAction(ActionEvent event)
    {
    }

    @FXML
    private void threeAction(ActionEvent event)
    {
    }

    @FXML
    private void multiAction(ActionEvent event)
    {
    }

    @FXML
    private void nineAction(ActionEvent event)
    {
    }

    @FXML
    private void eightAction(ActionEvent event)
    {
    }

    @FXML
    private void sevenAction(ActionEvent event)
    {
    }

    @FXML
    private void divideAction(ActionEvent event)
    {
    }

    @FXML
    private void moduloAction(ActionEvent event)
    {
    }

    @FXML
    private void deleteAction(ActionEvent event)
    {
    }

    @FXML
    private void plusMinusAction(ActionEvent event)
    {
    }
    
    public void setViewModel(CalculatorViewModel viewModel)
    {
        this.viewModel = viewModel;
    }

    @FXML
    private void dotAction(ActionEvent event)
    {
    }
}
